<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://contentpress.co
 * @since             1.0.0
 * @package           ContentPress
 *
 * @wordpress-plugin
 * Plugin Name:       ContentPress - Contribute
 * Plugin URI:        http://contentpress.co
 * Description:       ContentPress - Contribute provides fields to allow content editors to submit content ready for review for any page on a WordPress site
 * Version:           0.2
 * Author:            Thirty8 Digital
 * Author URI:        http://thirty8.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       contentpress-contribute
 * Domain Path:       /languages
 * Bitbucket Plugin URI: https://bitbucket.org/dmjellis/contentpress-contribute
 * Bitbucket Branch:  master
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/contentpress-contribute-activator.php';
	Plugin_Name_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/contentpress-contribute-deactivator.php';
	Plugin_Name_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_plugin_name' );
register_deactivation_hook( __FILE__, 'deactivate_plugin_name' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/contentpress-contribute.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

	$plugin = new Plugin_Name();
	$plugin->run();

}
run_plugin_name();


// Hide ACF admin

//add_filter('includes/advanced-custom-fields/settings/show_admin', '__return false');


// Add ACF fields

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_55dadabf63bd2',
	'title' => 'Draft Content Builder',
	'fields' => array (
		array (
			'key' => 'field_55dadad6f8efb',
			'label' => 'Draft Page Content',
			'name' => 'draft_page_content',
			'type' => 'flexible_content',
			'instructions' => 'Add your draft content here. Paste content from Word into a "rich text" field, or attach images or other documents using the "attachment" field.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'button_label' => '+ Add Draft Content',
			'min' => '',
			'max' => '',
			'layouts' => array (
				array (
					'key' => '55dadae6140db',
					'name' => 'richtext_content_block',
					'label' => 'Rich text content',
					'display' => 'block',
					'sub_fields' => array (
						array (
							'key' => 'field_55dadaf1f8efc',
							'label' => 'Richtext draft content',
							'name' => 'richtext_draft',
							'type' => 'wysiwyg',
							'instructions' => 'Use the richtext box to paste in any Word content. Use the text paste tool to get rid of any weird formatting. If there are images embedded in the text, these will have to be added separately using the image field.',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'tabs' => 'all',
							'toolbar' => 'full',
							'media_upload' => 0,
						),
					),
					'min' => '',
					'max' => '',
				),
				array (
					'key' => '55dadb64f8efd',
					'name' => 'attachment_content',
					'label' => 'Attachment content',
					'display' => 'row',
					'sub_fields' => array (
						array (
							'key' => 'field_55dadb6ef8efe',
							'label' => 'Attachment Content',
							'name' => 'attachment_content_block',
							'type' => 'file',
							'instructions' => 'Choose the image (or PDF) file you want to upload',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'library' => 'all',
							'min_size' => '',
							'max_size' => '',
							'mime_types' => '',
						),
					),
					'min' => '',
					'max' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;


/* WORKS

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_contentpress-contribute-fields',
		'title' => 'ContentPress Contribute fields',
		'fields' => array (
			array (
				'key' => 'field_55ad48444c5b2',
				'label' => 'Raw content',
				'name' => 'cp_raw_content',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'page',
					'order_no' => 0,
					'group_no' => 1,
				),
			),
		),
		'options' => array (
			'position' => 'acf_after_title',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

*/






?>
